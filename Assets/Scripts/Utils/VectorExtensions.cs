﻿using UnityEngine;

namespace Utils {
    public static class VectorExtensions {
        public static Vector3 ToBasePlane(this Vector3 v) {
            return new Vector3(v.x, v.y, 0);
        }
    }
}