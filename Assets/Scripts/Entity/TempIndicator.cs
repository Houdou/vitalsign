﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempIndicator : MonoBehaviour {
    public float AnchorPos = 0.05f;
    public float Distance = 1f;
    public float HeightOffset = 5f;

    public GameObject UpArrowPrefab;
    public GameObject DownArrowPrefab;

    private List<GameObject> indicators = new List<GameObject>();

    public void SetIndicator(int level) {
        if (level == 1) {
            return;
        }

        var screen_width = GameMaster.Instance.ScreenPosMax.x - GameMaster.Instance.ScreenPosMin.x;

        if (level > 1) {
            for (var i = 0; i < level - 1; i++) {
                var new_indicator = Instantiate(UpArrowPrefab, transform);
                new_indicator.transform.localPosition =
                    new Vector3(AnchorPos * screen_width + i * Distance, HeightOffset);
                indicators.Add(new_indicator);
            }
        } else {
            for (var i = 0; i < -level + 1; i++) {
                var new_indicator = Instantiate(DownArrowPrefab, transform);
                new_indicator.transform.localPosition =
                    new Vector3(AnchorPos * screen_width + i * Distance, -HeightOffset);
                indicators.Add(new_indicator);
            }
        }
    }

    public void ResetIndicator() {
        foreach (var indicator in indicators) {
            Destroy(indicator);
        }
        indicators = new List<GameObject>();
    }
}