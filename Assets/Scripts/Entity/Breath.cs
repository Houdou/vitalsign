﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public enum BreathType {
    [Description("O2")] O2,
    [Description("CO2")] CO2,
}

[RequireComponent(typeof(SpriteRenderer))]
public class Breath : MonoBehaviour {
    public BreathType Type;
    public Vector2 Pos;
    public SpriteRenderer sr;

    private float currentAlpha = 0.9f;
    private float targetAlpha = 0.9f;

    private void Update() {
        if (Mathf.Abs(currentAlpha - targetAlpha) > 0.001f) {
            currentAlpha = Mathf.Lerp(currentAlpha, targetAlpha, 3f * Time.deltaTime);

            var sr_color = sr.color;
            sr_color.a = currentAlpha;
            sr.color = sr_color;
        }
    }

    public void FadeOut() {
        targetAlpha = 0f;
    }
}