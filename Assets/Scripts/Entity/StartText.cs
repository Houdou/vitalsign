﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StartText : MonoBehaviour {
    public CanvasGroup group;
    public TextMeshProUGUI text;

    public float StartAlpha = 1f;
    public float TransitionFactor = 12f;

    private float currentAlpha = 1f;
    private float targetAlpha = 1f;

    private void Start() {
        currentAlpha = StartAlpha;
        targetAlpha = StartAlpha;
        group.alpha = StartAlpha;
    }

    private void Update() {
        if (Mathf.Abs(currentAlpha - targetAlpha) > 0.001f) {
            currentAlpha = Mathf.Lerp(currentAlpha, targetAlpha, TransitionFactor * Time.deltaTime);

            group.alpha = currentAlpha;
        }
    }

    public void UpdateText(string str) {
        text.text = str;
    }

    IEnumerator DelayFade(float alpha) {
        yield return new WaitForSeconds(0.1f);
        targetAlpha = alpha;
    }

    public void FadeOut() {
        StartCoroutine(DelayFade(0f));
    }

    public void FadeIn() {
        StartCoroutine(DelayFade(1f));
    }
}