﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BreathContainer : MonoBehaviour {
    public BreathType Type;
    public GameObject ItemPrefab;
    public event Action<int, BreathType> OnHit;

    private List<Breath> items = new List<Breath>();
    private List<Breath> prevItems = new List<Breath>();

    private void Start() {
        OnHit += (hitIndex, type) => {
            if (hitIndex < 0 || hitIndex >= items.Count) {
                return;
            }

            var item = items[hitIndex];
            items.RemoveAt(hitIndex);
            item.FadeOut();
            Destroy(item.gameObject, 1f);
        };
    }

    public void AddBreathAt(float pos) {
        var screen_width = GameMaster.Instance.ScreenPosMax.x - GameMaster.Instance.ScreenPosMin.x;
        var pos_x = pos * screen_width + GameMaster.Instance.ScreenPosMin.x;

        var new_item_obj = Instantiate(ItemPrefab, transform);
        new_item_obj.transform.localPosition = new Vector3(pos_x, 0f);

        var new_item = new_item_obj.GetComponent<Breath>();
        new_item.Pos = new Vector2(pos, Type == BreathType.O2 ? .95f : 0.05f);
        items.Add(new_item);
    }

    public bool Hit(Vector2 pos, float threshold) {
        var hit = false;
        var hit_index = -1;
        var d = threshold;
        foreach (var (item, index) in items.Select((item, index) => (item, index))) {
            var new_d = Vector2.Distance(pos, item.Pos);
            if (new_d >= d) continue;

            hit = true;
            hit_index = index;
            d = new_d;
        }

        if (hit) {
            OnHit?.Invoke(hit_index, Type);
        }

        return hit;
    }

    #region Cycle

    public void SwapItems() {
        foreach (var item in prevItems) {
            item.FadeOut();
            Destroy(item.gameObject, 1f);
        }

        while(items.Count > 0) {
            var item = items[0];
            items.RemoveAt(0);
            prevItems.Add(item);
        }
    }

    public void ResetBreath() {
        foreach (var item in items) {
            Destroy(item);
        }
        foreach (var item in prevItems) {
            Destroy(item);
        }

        items = new List<Breath>();
        prevItems = new List<Breath>();
    }

    #endregion
}