﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Heart : MonoBehaviour {
    public float Pos;
    public SpriteRenderer sr;
    public SpriteRenderer srb;

    private float currentAlpha = 0.9f;
    private float targetAlpha = 0.9f;

    private void Start() {
        sr.enabled = false;
    }
    
    private void Update() {
        if (Mathf.Abs(currentAlpha - targetAlpha) > 0.001f) {
            currentAlpha = Mathf.Lerp(currentAlpha, targetAlpha, 12f * Time.deltaTime);

            var sr_color = sr.color;
            sr_color.a = currentAlpha;
            sr.color = sr_color;
            srb.color = sr_color;
        }
    }

    public void Fix() {
        sr.enabled = true;
        srb.enabled = false;
    }

    IEnumerator DelayFadeOut() {
        yield return new WaitForSeconds(0.1f);
        targetAlpha = 0f;
    }

    public void FadeOut() {
        StartCoroutine(DelayFadeOut());
    }
}