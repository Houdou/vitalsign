﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HeartContainer : MonoBehaviour {
    public GameObject HeartPrefab;

    public event Action<int> OnHit;

    private List<Heart> items = new List<Heart>();
    private List<Heart> prevItems = new List<Heart>();

    private void Start() {
        OnHit += hitIndex => {
            if (hitIndex < 0 || hitIndex >= items.Count) {
                return;
            }

            var item = items[hitIndex];
            items.RemoveAt(hitIndex);
            item.Fix();
            item.FadeOut();
            Destroy(item.gameObject, 1f);
        };
    }

    public void AddHeartAt(float progress) {
        var screen_width = GameMaster.Instance.ScreenPosMax.x - GameMaster.Instance.ScreenPosMin.x;
        var pos_x = progress * screen_width + GameMaster.Instance.ScreenPosMin.x;

        var new_item_obj = Instantiate(HeartPrefab, transform);
        new_item_obj.transform.localPosition = new Vector3(pos_x, 0f);

        var new_item = new_item_obj.GetComponent<Heart>();
        new_item.Pos = progress;
        items.Add(new_item);
    }

    public bool Hit(float pos, float threshold) {
        var hit = false;
        var hit_index = -1;
        var d = threshold;
        foreach (var (item, index) in items.Select((item, index) => (item, index))) {
            var new_d = Mathf.Abs(pos - item.Pos);
            if (new_d >= d) continue;

            hit = true;
            hit_index = index;
            d = new_d;
        }

        if (hit) {
            OnHit?.Invoke(hit_index);
        }

        return hit;
    }

    public void SwapItems() {
        foreach (var item in prevItems) {
            item.FadeOut();
            Destroy(item.gameObject, 1f);
        }

        while(items.Count > 0) {
            var item = items[0];
            items.RemoveAt(0);
            prevItems.Add(item);
        }
    }

    public void ResetItems() {
        foreach (var item in items) {
            Destroy(item);
        }
        foreach (var item in prevItems) {
            Destroy(item);
        }
        
        items = new List<Heart>();
        prevItems = new List<Heart>();
    }
}
