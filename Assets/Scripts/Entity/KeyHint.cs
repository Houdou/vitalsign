﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class KeyHint : MonoBehaviour {
    public Image sr;

    private float currentAlpha = 0.9f;
    private float targetAlpha = 0.9f;

    private void Start() {
        var color = sr.color;
        color.a = 0f;
        currentAlpha = 0f;
        targetAlpha = 0f;
        sr.color = color;
    }

    private void Update() {
        if (Mathf.Abs(currentAlpha - targetAlpha) > 0.001f) {
            currentAlpha = Mathf.Lerp(currentAlpha, targetAlpha, 12f * Time.deltaTime);

            var sr_color = sr.color;
            sr_color.a = currentAlpha;
            sr.color = sr_color;
        }
    }

    IEnumerator DelayFade(float alpha) {
        yield return new WaitForSeconds(0.1f);
        targetAlpha = alpha;
    }

    public void FadeOut() {
        if (enabled) {
            StartCoroutine(DelayFade(0f));
        }
    }

    public void FadeIn() {
        if (enabled) {
            StartCoroutine(DelayFade(1f));
        }
    }
}