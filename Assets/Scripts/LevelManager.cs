﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour {
    public StartText StartText;
    public StartText CreditText;
    public StartText ScoreText;

    [SerializeField] protected List<LineRunner> lrs = new List<LineRunner>();

    [SerializeField] protected List<Challenger> chs = new List<Challenger>();

    [SerializeField] protected List<GameObject> panels = new List<GameObject>();
    [SerializeField] protected List<GameObject> leftPanels = new List<GameObject>();

    public bool Started { get; private set; }
    public bool IsGameOver { get; private set; }
    public AudioSource GameOverSound;

    public int Level { get; set; }
    public float GameTimer { get; private set; }
    
    private void Start() {
        UpdateArrangement(0);
    }

    private void Update() {
        if (!GameMaster.Instance.Paused && Started) {
            GameTimer += Time.deltaTime;
        }

        if (!Started) {
            if (Input.GetKeyDown(KeyCode.Return)) {
                StartLevel();
                StartText.FadeOut();
            }
        }

        if (Application.isEditor) {
            if (Input.GetKeyDown(KeyCode.I)) {
                UpdateArrangement(1);
            }
            if (Input.GetKeyDown(KeyCode.O)) {
                UpdateArrangement(2);
            }
            if (Input.GetKeyDown(KeyCode.P)) {
                UpdateArrangement(3);
            }
            
            if (Input.GetKeyDown(KeyCode.Q)) {
                chs[0].Challenge();
            }
            if (Input.GetKeyDown(KeyCode.W)) {
                chs[1].Challenge();
            }
            if (Input.GetKeyDown(KeyCode.E)) {
                chs[2].Challenge();
            }
        }

        var factor = GetDangerFactor();
        targetDangerFactor = Mathf.Lerp(0f, 1f, Mathf.Max(0f, 2f * factor - 1f));
        UpdateDangerMask();

        if (targetDangerFactor >= 1f && !IsGameOver) {
            GameOver();
        }
    }


    #region Level
    public event Action OnStart;

    private void StartLevel() {
        GameOverSound.Stop();
        foreach (var lr in lrs) {
            lr.Reset();
        }

        foreach (var ch in chs) {
            ch.Reset();
        }
        // Start
        Level = 1;
        Started = true;
        IsGameOver = false;
        ScoreText.UpdateText("");
        OnStart?.Invoke();
    }

    public bool LevelUp() {
        Level++;
        return Level <= 5;
    }
    
    private void GameOver() {
        IsGameOver = true;
        GameOverSound.PlayOneShot(GameOverSound.clip);
        foreach (var lr in lrs) {
            lr.Stop();
        }
        ScoreText.UpdateText($"You kept it alive for <color #F1243A>{GameTimer * GameMaster.Instance.RefreshRate:0.#}</color> days.");
        
        StartCoroutine(DelayedStartOver());
    }

    IEnumerator DelayedStartOver() {
        // Show GameOver
        ScoreText.FadeIn();
        yield return new WaitForSeconds(1f);
        StartText.UpdateText("Again?");
        StartText.FadeIn();
        yield return new WaitForSeconds(1f);
        CreditText.FadeIn();
        Started = false;
    }

    #endregion

    #region LineRunnerArrangement

    public Vector2[] GetLineRunnerArrangement(int count) {
        var arr = new Vector2[count];
        var split = new float[count + 1];
        for (int i = 0; i <= count; i++) {
            split[i] = i / (float) count;
        }

        for (var i = 0; i < count; i++) {
            arr[i] = new Vector2(1f - (split[i] + split[i + 1]) * 0.5f, 1f / count);
        }

        return arr;
    }

    public void UpdateArrangement(int level) {
        if (level > lrs.Count) {
            level = lrs.Count;
        }

        if (level == 0) {
            foreach (var lr in lrs) {
                lr.CurrentLineCenter = -1f;
                lr.TargetLineCenter = -1f;
                lr.CurrentLineHeight = 0.5f;
                lr.TargetLineHeight = 0.5f;
                lr.Enabled = false;
            }

            foreach (var panel in panels) {
                panel.SetActive(false);
            }

            foreach (var panel in leftPanels) {
                panel.SetActive(false);
            }

            return;
        }

        var arr = GetLineRunnerArrangement(level);
        for (int i = 0; i < level; i++) {
            lrs[i].TargetLineCenter = arr[i].x;
            lrs[i].TargetLineHeight = arr[i].y;
            lrs[i].Enabled = true;
            panels[i].SetActive(true);
            leftPanels[i].SetActive(true);
        }

        for (int i = level; i < lrs.Count; i++) {
            lrs[i].TargetLineCenter = -1f;
            lrs[i].TargetLineHeight = 0.5f;
            lrs[i].Enabled = false;
            panels[i].SetActive(false);
            leftPanels[i].SetActive(false);
        }
    }

    #endregion

    #region Challenge

    public void CreateChallenge() {
        var chosen_index = Random.Range(0, chs.Count - 1);
        chs[chosen_index].Challenge();
    }

    #endregion

    #region DangerMask

    public Image Mask;
    private float currentDangerFactor = 0f;
    private float targetDangerFactor = 1f;

    private float GetDangerFactor() {
        if (IsGameOver) {
            return 1f;
        }
        return Mathf.Max(chs.Select(ch => ch.GetDangerFactor()).ToArray());
    }

    private void UpdateDangerMask() {
        if (Mathf.Abs(currentDangerFactor - targetDangerFactor) > 0.001f) {
            currentDangerFactor = Mathf.Lerp(currentDangerFactor, targetDangerFactor, 4.0f * Time.deltaTime);
            var mask_color = Mask.color;
            mask_color.a = currentDangerFactor / 2f;
            Mask.color = mask_color;
        }
    }

    #endregion
}