using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class TempLineRunner : LineRunner {
    public event Action<int> OnCursorUpdate;

    protected override void Update() {
        base.Update();

        UpdateCursor();
    }

    public AudioSource SFX;

    private float audioTimer = 0f;

    protected override void HandleInput() {
        base.HandleInput();

        if (audioTimer > -2f) {
            audioTimer -= Time.deltaTime;
        }


        if (Input.GetButtonDown("BTD")) {
            MoveCursor(-1);
            PlaySFX();
        }

        if (Input.GetButtonDown("BTU")) {
            MoveCursor(1);
        }
    }

    private void PlaySFX() {
        if (audioTimer > 0f) {
            return;
        }

        SFX.PlayOneShot(SFX.clip);

        audioTimer = 2f;
    }

    public override string GetCurrentReadString() {
        var value = CurrentValue;
        var temp = 36.7f + (value.y - 0.5f) * (value.y > 0.5f ? 8f : 2f);
        return $"<sub>↑40.7</sub>\n{temp:0.#}°C\n<sup>↓35.7</sup>";
    }

    public override void Reset() {
        base.Reset();
        currentCursor = 1f;
        currentTargetCursor = 1;
        previousCursor = 0.5f;
    }

    #region Cursor

    private int previousIndex = -1;
    private float previousCursor = 0.5f;

    private int currentTargetCursor = 1;

    public int CurrentTargetCursor => currentTargetCursor;

    private float currentCursor = 1f;
    public float CursorSmoothingFactor = 0.5f;

    public void MoveCursor(int direction) {
        currentTargetCursor += direction;
        OnCursorUpdate?.Invoke(currentTargetCursor);
    }

    public void MoveCursorTo(int value) {
        currentTargetCursor = value;
        OnCursorUpdate?.Invoke(currentTargetCursor);
    }

    private void UpdateCursor() {
        if (Mathf.Abs(currentTargetCursor - currentCursor) > 0.001f) {
            // Random walk
            float step;
            float rng = Random.Range(0f, 1f);
            if (rng > 0.3f) {
                step = CursorSmoothingFactor;
            } else if (rng < 0.1f) {
                step = -CursorSmoothingFactor;
            } else {
                step = 0f;
            }

            if (currentTargetCursor == 1) {
                step *= 5f;
            }

            currentCursor = Mathf.Clamp(Mathf.Lerp(currentCursor, currentTargetCursor, step * Time.deltaTime), 0f, 2f);
        }
    }

    #endregion

    protected override void UpdateLinePos() {
        base.UpdateLinePos();

        var next_index_unclamped = Mathf.CeilToInt((Progress + 0.05f) * GameMaster.RESOLUTION);
        int next_index;
        if (next_index_unclamped >= GameMaster.RESOLUTION) {
            next_index = GameMaster.RESOLUTION - 1;
        } else {
            next_index = next_index_unclamped;
        }

        if (previousIndex > next_index) {
            previousIndex = -1;
        }

        for (var i = previousIndex + 1; i <= next_index; i++) {
            if (i < 0 || i >= GameMaster.RESOLUTION) {
                continue;
            }

            var pos_x = Mathf.LerpUnclamped(0, SpaceSize.x, i / (GameMaster.RESOLUTION - 1.0f));
            var step = (i - previousIndex) / ((float) next_index - previousIndex);
            var jiggle_range = 0.004f / CurrentLineHeight;
            var jiggle = Random.Range(1 - jiggle_range, 1 + jiggle_range);
            var height = Mathf.Lerp(previousCursor, currentCursor / 2.0f, step) * jiggle;
            if (Flipped) {
                LinePosPrev[i] = new Vector3(pos_x, height, 0f);
            } else {
                LinePosCur[i] = new Vector3(pos_x, height, 0f);
            }
        }

        previousIndex = next_index;
        previousCursor = currentCursor / 2.0f;
    }
}