using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class ECGLineRunner : LineRunner {
    public AnimationCurve BeatShape;
    [Range(0.01f, 1f)] public float BeatWidth;
    [Range(0.01f, 1f)] public float BeatHeight;

    public AudioSource SFX;
    
    public event Action<float> OnBeat;
    
    protected override void HandleInput() {
        base.HandleInput();
        
        if (Input.GetButtonDown("Beat")) {
            if (Flipped) {
                MarkBeat(ref LinePosPrev, ref LinePosCur, Progress, BeatWidth);
            } else {
                MarkBeat(ref LinePosCur, ref LinePosPrev, Progress, BeatWidth);
            }
            OnBeat?.Invoke(Progress);
            SFX.PlayOneShot(SFX.clip);
        }

    }

    public void MarkBeat(ref Vector3[] linePos, ref Vector3[] nextLinePos, float pos, float width) {
        var begin_index = GetResolutionIndexUnclamped(pos);
        var end_index = GetResolutionIndexUnclamped(pos + width);
        var length = end_index - begin_index + 1;
        for (var i = begin_index; i <= end_index; i++) {
            if (i < 0) {
                continue;
            }
            
            var pos_x = Mathf.Lerp(0, SpaceSize.x, i / (GameMaster.RESOLUTION - 1.0f));

            if (length == 1) {
                var single_height = BeatShape.Evaluate(pos) * BeatHeight;
                linePos[begin_index] = new Vector3(pos_x, single_height + 0.5f, 0f);
                continue;
            }

            var cursor = (i - begin_index) / (length - 1.0f);
            var jiggle_range = Mathf.Abs(cursor - 0.5f) * 0.004f + 0.001f;
            var jiggle = Random.Range(-jiggle_range, jiggle_range);
            var height = (BeatShape.Evaluate(cursor + Random.Range(-0.001f, 0.001f)) + jiggle) * BeatHeight;
            
            if (i >= GameMaster.RESOLUTION) {
                var mod_index = i - GameMaster.RESOLUTION;
                var pos_x_shifted = Mathf.Lerp(0, SpaceSize.x, mod_index / (GameMaster.RESOLUTION - 1.0f));
                nextLinePos[mod_index] = new Vector3(pos_x_shifted, height + 0.5f, 0f);
                ProtectedIndex = Mathf.Max(ProtectedIndex, i - GameMaster.RESOLUTION);
                continue;
            }

            linePos[i] = new Vector3(pos_x, height + 0.5f, 0f);
        }
    }
}