using System;
using UnityEngine;

public class RRLineRunner : LineRunner {
    public event Action<Vector2> OnCursor;
    public float RRDeviationForce = 15f;
    
    protected override void Update() {
        base.Update();

        UpdateCursor();
        OnCursor?.Invoke(new Vector2(Progress, CurrentValue.y));
    }

    public override void Stop() {
        previousCursor = 0.5f;
        currentCursor = 0.5f;
        currentTargetCursor = 0.5f;
        base.Stop();
    }

    public AudioSource SFX;
    
    protected override void HandleInput() {
        base.HandleInput();

        var diff = Input.GetAxis("Vertical");
        if (Mathf.Abs(diff) > 0.001f) {
            MoveCursor(diff);
        }
        
        // Deviate from center
        if (currentCursor >= 0.5f) {
            MoveCursor(RRDeviationForce * Time.deltaTime * Mathf.Abs(1f - currentCursor));
        } else {
            MoveCursor(-RRDeviationForce * Time.deltaTime * Mathf.Abs(currentCursor));
        }

        if (currentCursor >= 0.5f && previousCursor < 0.5f) {
            SFX.PlayOneShot(SFX.clip);
        }
    }

    private int previousIndex = -1;
    private float previousCursor = 0.5f;
    protected override void UpdateLinePos() {
        base.UpdateLinePos();
        
        var next_index_unclamped = Mathf.CeilToInt((Progress + 0.05f) * GameMaster.RESOLUTION);
        int next_index;
        if(next_index_unclamped >= GameMaster.RESOLUTION) {
            next_index = GameMaster.RESOLUTION - 1;
        } else {
            next_index = next_index_unclamped;
        }
        
        if (previousIndex > next_index) {
            previousIndex = -1;
        }
        for (var i = previousIndex + 1; i <= next_index; i++) {
            if (i < 0 || i >= GameMaster.RESOLUTION) {
                continue;
            }
            
            var pos_x = Mathf.LerpUnclamped(0, SpaceSize.x, i / (GameMaster.RESOLUTION - 1.0f));
            var step = (i - previousIndex) / ((float)next_index - previousIndex);
            var height = Mathf.Lerp(previousCursor, currentCursor, step);
            if (Flipped) {
                LinePosPrev[i] = new Vector3(pos_x, height, 0f);
            } else {
                LinePosCur[i] = new Vector3(pos_x, height, 0f);
            }
        }

        previousIndex = next_index;
        previousCursor = currentCursor;
    }

    #region Cursor

    [Range(1f, 8f)]
    public float CursorSpeed = 4f;

    private float currentTargetCursor = 0.5f;
    private float currentCursor = 0.5f;
    public float CursorSmoothingFactor = 4f;
    
    public void MoveCursor(float direction) {
        currentTargetCursor += Time.deltaTime * CursorSpeed * direction;
        currentTargetCursor = Mathf.Clamp01(currentTargetCursor);
    }

    private void UpdateCursor() {
        if (Mathf.Abs(currentTargetCursor - currentCursor) > 0.001f) {
            currentCursor = Mathf.Lerp(currentCursor, currentTargetCursor, CursorSmoothingFactor * Time.deltaTime);
        }
    }
    #endregion
}