using UnityEngine;

public class NIBPLineRunner : LineRunner {
    protected override void Update() {
        FillNIBPData();
        
        base.Update();
        
        // NIBP will shift cursor to abnormal status
        UpdateCursor();
    }


    protected override void HandleInput() {
        base.HandleInput();
    }

    #region NIBP
    public AnimationCurve BeatShape;
    [Range(0.01f, 1f)] public float BeatWidth;
    [Range(0.01f, 1f)] public float BeatHeight;

    public float NIBPInterval = 0.2f;
    
    private float nibpTimer = 0f;
    private void FillNIBPData() {
        nibpTimer -= Time.deltaTime;
        if (nibpTimer < 0f) {
            // Trigger
            if (Flipped) {
                MarkBeat(ref LinePosPrev, ref LinePosCur, Progress, BeatWidth);
            } else {
                MarkBeat(ref LinePosCur, ref LinePosPrev, Progress, BeatWidth);
            }

            nibpTimer += NIBPInterval;
        }
    }

    public void MarkBeat(ref Vector3[] linePos, ref Vector3[] nextLinePos, float pos, float width) {
        var begin_index = GetResolutionIndexUnclamped(pos);
        var end_index = GetResolutionIndexUnclamped(pos + width);
        var length = end_index - begin_index + 1;
        for (var i = begin_index; i <= end_index; i++) {
            if (i < 0) {
                continue;
            }
            
            var pos_x = Mathf.Lerp(0, SpaceSize.x, i / (GameMaster.RESOLUTION - 1.0f));

            if (length == 1) {
                var single_height = BeatShape.Evaluate(pos) * BeatHeight;
                linePos[begin_index] = new Vector3(pos_x, single_height + currentCursor, 0f);
                continue;
            }

            var cursor = (i - begin_index) / (length - 1.0f);
            var jiggle_range = Mathf.Abs(cursor - 0.5f) * 0.004f + 0.001f;
            var jiggle = Random.Range(-jiggle_range, jiggle_range);
            var height = (BeatShape.Evaluate(cursor + Random.Range(-0.001f, 0.001f)) + jiggle) * BeatHeight;
            
            if (i >= GameMaster.RESOLUTION) {
                var mod_index = i - GameMaster.RESOLUTION;
                var pos_x_shifted = Mathf.Lerp(0, SpaceSize.x, mod_index / (GameMaster.RESOLUTION - 1.0f));
                nextLinePos[mod_index] = new Vector3(pos_x_shifted, height + currentCursor, 0f);
                ProtectedIndex = Mathf.Max(ProtectedIndex, i - GameMaster.RESOLUTION);
                continue;
            }

            linePos[i] = new Vector3(pos_x, height + currentCursor, 0f);
        }
    }

    #endregion

    #region Cursor

    private int previousIndex = -1;
    private float previousCursor = 0.5f;
    
    private float currentTargetCursor = 0.5f;
    private float currentCursor = 0.5f;
    public float CursorSmoothingFactor = 0.5f;
    
    public void MoveCursor(float direction) {
        currentTargetCursor += direction;
    }

    private void UpdateCursor() {
        if (Mathf.Abs(currentTargetCursor - currentCursor) > 0.001f) {
            currentCursor = Mathf.Lerp(currentCursor, currentTargetCursor, CursorSmoothingFactor * Time.deltaTime);
        }
    }
    
    #endregion
}