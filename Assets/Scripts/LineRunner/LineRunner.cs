﻿using System;
using UnityEngine;

public class LineRunner : MonoBehaviour {
    public LineRenderer lrp; // Previous
    public LineRenderer lrc; // Current

    protected Vector2 SpaceSize;

    public float CurrentLineCenter = 0.5f;
    public float TargetLineCenter = 0.5f;
    public float CurrentLineHeight = 0.5f;
    public float TargetLineHeight = 0.5f;
    public float LineShiftSmoothingFactor = 4.0f;
    public float LocalHeight = 1f;

    public bool Enabled;

    protected virtual void Start() {
        SpaceSize = GameMaster.Instance.ScreenPosMax - GameMaster.Instance.ScreenPosMin;
        ResetLinePos(ref LinePosPrev);
        ResetLinePos(ref LinePosCur);

        InitLineRenderer(lrp);
        InitLineRenderer(lrc);

        SetupProgressReset();
    }


    protected virtual void Update() {
        if (Enabled) {
            HandleInput();
        }

        UpdateLinePos();

        // Lerp line
        if (Mathf.Abs(TargetLineCenter - CurrentLineCenter) > 0.0001f) {
            CurrentLineCenter =
                Mathf.Lerp(CurrentLineCenter, TargetLineCenter, LineShiftSmoothingFactor * Time.deltaTime);
        }

        if (Mathf.Abs(TargetLineHeight - CurrentLineHeight) > 0.0001f) {
            CurrentLineHeight =
                Mathf.Lerp(CurrentLineHeight, TargetLineHeight, LineShiftSmoothingFactor * Time.deltaTime);
        }

        // Render
        if (!GameMaster.Instance.Paused) {
            UpdateProgress();

            RenderCurrentLine(Progress);
            RenderPreviousLine(Progress);
        }
    }


    protected virtual void HandleInput() {
    }

    protected virtual void UpdateLinePos() {
    }

    #region Progress

    public float Progress { get; private set; }

    public Vector3 CurrentValue {
        get {
            var index = GetResolutionIndex(Progress);
            return Flipped ? LinePosPrev[index] : LinePosCur[index];
        }
    }

    public virtual string GetCurrentReadString() {
        return $"{CurrentValue.y}";
    } 

    public event Action OnProgressReset;

    protected virtual void UpdateProgress() {
        Progress += Time.deltaTime * GameMaster.Instance.RefreshRate;
        if (Progress > 1.0f) {
            OnProgressReset?.Invoke();
            Progress -= 1.0f;
        }
    }

    protected virtual void SetupProgressReset() {
        OnProgressReset += () => {
            if (Flipped) {
                ResetLinePos(ref LinePosCur);
            } else {
                ResetLinePos(ref LinePosPrev);
            }

            ProtectedIndex = -1;
            Flipped = !Flipped;
        };
    }

    #endregion

    #region LinePos

    protected Vector3[] LinePosPrev = new Vector3[GameMaster.RESOLUTION];
    protected Vector3[] LinePosCur = new Vector3[GameMaster.RESOLUTION];
    public bool Flipped { get; private set; }
    protected int ProtectedIndex = -1;

    public int GetResolutionIndex(float progress) {
        return Mathf.RoundToInt(Mathf.Clamp01(progress) * (GameMaster.RESOLUTION - 1));
    }

    public int GetResolutionIndexUnclamped(float progress) {
        return Mathf.RoundToInt(progress * (GameMaster.RESOLUTION - 1));
    }

    public Vector3 GetLinePos(ref Vector3[] pos, float progress, float offset) {
        return Vector3.zero;
    }

    protected virtual void ResetLinePos(ref Vector3[] pos, float baseHeight = 0.5f) {
        for (var i = 0; i < pos.Length; i++) {
            if (i < ProtectedIndex) {
                continue;
            }

            pos[i] = new Vector3(Mathf.Lerp(0f, SpaceSize.x, i / (GameMaster.RESOLUTION - 1.0f)),
                baseHeight, 0f);
        }
    }

    #endregion

    #region Line Render

    private void InitLineRenderer(LineRenderer lr) {
        lr.transform.position = new Vector3(SpaceSize.x * -0.5f, SpaceSize.y * -0.5f, 0f);

        lr.positionCount = GameMaster.RESOLUTION;
        var pos = new Vector3[GameMaster.RESOLUTION];
        for (var i = 0; i < pos.Length; i++) {
            pos[i] = LinePosPrev[i];
        }

        lr.SetPositions(pos);
    }

    private void RenderPreviousLine(float progress) {
        if (Flipped) {
            RenderLine(lrp, ref LinePosCur, progress);
        } else {
            RenderLine(lrp, ref LinePosPrev, progress);
        }
    }

    private void RenderCurrentLine(float progress) {
        if (Flipped) {
            RenderLine(lrc, ref LinePosPrev, progress - 1f);
        } else {
            RenderLine(lrc, ref LinePosCur, progress - 1f);
        }
    }

    private void RenderLine(LineRenderer lr, ref Vector3[] linePos, float offset) {
        var pos = new Vector3[GameMaster.RESOLUTION];
        for (var i = 0; i < pos.Length; i++) {
            // - (1-progress) ~ progress
            var cursor = i / (pos.Length - 1.0f) + offset;
            if (cursor < -0.01f || cursor > 1.01f) {
                pos[i] = new Vector3(Mathf.LerpUnclamped(0, SpaceSize.x, cursor), linePos[0].y, 0f);
            } else {
                var index = Mathf.RoundToInt(Mathf.Clamp01(cursor) * (GameMaster.RESOLUTION - 1));
                pos[i] = linePos[index];
            }

            pos[i].y = ((pos[i].y - 0.5f) * CurrentLineHeight * LocalHeight + CurrentLineCenter) * SpaceSize.y;
        }

        lr.SetPositions(pos);
    }

    #endregion

    public virtual void Stop() {
        Enabled = false;
        ResetLinePos(ref LinePosCur);
        ResetLinePos(ref LinePosPrev);
    }

    public virtual void Reset() {
        ResetLinePos(ref LinePosCur);
        ResetLinePos(ref LinePosPrev);
        CurrentLineCenter = -1f;
        TargetLineCenter = -1f;
        CurrentLineHeight = 0.5f;
        TargetLineHeight = 0.5f;
    }
}