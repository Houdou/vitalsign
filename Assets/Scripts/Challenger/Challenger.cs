﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public abstract class Challenger : MonoBehaviour {
    protected bool AutoChallenge = false;
    public bool AutoChallengeEnabled {
        get => AutoChallenge;
        set {
            if (!AutoChallenge && value) {
                ResetTimer();
            }

            if (!value) {
                challengeTimer = 999f;
            }

            AutoChallenge = value;
        }
    }

    public bool AutoVerifyEnabled;

    private float challengeTimer = 0f;
    public float ChallengeIntervalMax = 999f;
    public float ChallengeIntervalMin = 999f;

    private float verifyTimer = 0f;
    public float VerifyInterval = 1f;

    public event Action<ChallengeType> OnChallengeSuccess;
    public event Action<ChallengeType> OnChallengeFailed;

    [SerializeField] public List<KeyHint> Hints = new List<KeyHint>();

    protected ChallengeType Type = ChallengeType.BT;

    protected virtual void Update() {
        if (AutoChallengeEnabled) {
            challengeTimer -= Time.deltaTime;
            if (challengeTimer < 0f) {
                Challenge();
                challengeTimer += Random.Range(ChallengeIntervalMin, ChallengeIntervalMax);
            }
        }

        if (AutoVerifyEnabled) {
            verifyTimer -= Time.deltaTime;
            if (verifyTimer < 0f) {
                var result = Verify();
                if (result) {
                    OnChallengeSuccess?.Invoke(Type);
                } else {
                    OnChallengeFailed?.Invoke(Type);
                }

                verifyTimer += VerifyInterval;
            }
        }
    }

    public virtual void Challenge() {
        foreach (var key_hint in Hints) {
            if (key_hint.enabled) {
                key_hint.FadeIn();
            }
        }
    }

    public void ResetTimer() {
        challengeTimer = Random.Range(ChallengeIntervalMin, ChallengeIntervalMax);
    }

    public virtual bool Verify() {
        return true;
    }

    public abstract float GetDangerFactor();
    public abstract string GetCurrentReadString();

    public abstract void Reset();
}