using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class TempChallenger : Challenger {
    public int TempChallengeMax = 5;
    public int TempChallengeMin = -2;

    public TempLineRunner lr;
    public TempIndicator TempIndicators;

    private bool firstTime = true;

    public override void Challenge() {
        if (!lr.Enabled) {
            return;
        }

        base.Challenge();

        // Easy start~
        if (firstTime) {
            lr.MoveCursorTo(2);
            TempIndicators.ResetIndicator();
            TempIndicators.SetIndicator(2);
            firstTime = false;
            return;
        }

        var value = Random.Range(TempChallengeMin, TempChallengeMax);
        lr.MoveCursorTo(value);
        TempIndicators.ResetIndicator();
        TempIndicators.SetIndicator(value);
    }

    public override bool Verify() {
        var value = lr.CurrentValue;
        return value.y <= 0.6f && value.y >= 0.4f;
    }

    public override float GetDangerFactor() {
        return lr.Enabled ? Mathf.Abs(lr.CurrentValue.y - 0.5f) * 2f : 0f;
    }

    public override string GetCurrentReadString() {
        return lr.GetCurrentReadString();
    }

    public override void Reset() {
        TempIndicators.ResetIndicator();
    }

    private float screenHeight;

    private void Start() {
        screenHeight = GameMaster.Instance.ScreenPosMax.y - GameMaster.Instance.ScreenPosMin.y;
        lr.OnProgressReset += () => { };
        lr.OnCursorUpdate += cursor => {
            TempIndicators.ResetIndicator();
            TempIndicators.SetIndicator(cursor);
        };
    }

    protected override void Update() {
        base.Update();

        UpdateTempIndicatorsPos();
    }

    public float CurrentLineCenter = -1f;

    private void UpdateTempIndicatorsPos() {
        if (Mathf.Abs(CurrentLineCenter - lr.TargetLineCenter) > 0.001f) {
            CurrentLineCenter = Mathf.Lerp(CurrentLineCenter, lr.CurrentLineCenter,
                lr.LineShiftSmoothingFactor * Time.deltaTime);
            var new_pos = new Vector3(0f, CurrentLineCenter * screenHeight + GameMaster.Instance.ScreenPosMin.y, 0);
            TempIndicators.transform.position = new_pos;
        }
    }
}