using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RRChallenger : Challenger {
    public BreathContainer O2Container;
    public BreathContainer CO2Container;
    public int BreathChallengeMax = 5;
    public int BreathChallengeMin = 2;
    public RRLineRunner lr;

    public int O2Count { get; private set; }
    public int CO2Count { get; private set; }

    private List<float> GeneratePos(int count) {
        if (count <= 1) {
            return new List<float> {0.0f};
        }

        var cur = 0f;
        var list = new List<float>();
        for (var i = 0; i < count; i++) {
            list.Add(cur);
            cur += 1f / (count - 1f);
        }

        return list;
    }

    public override void Challenge() {
        if (!lr.Enabled) {
            return;
        }
        
        base.Challenge();
        var o2_count = Random.Range(BreathChallengeMin, BreathChallengeMax);
        GenerateBreath(BreathType.O2, Random.Range(0.1f, 0.4f), Random.Range(0.6f, 0.95f), o2_count);
        var co2_count = Random.Range(BreathChallengeMin, BreathChallengeMax);
        GenerateBreath(BreathType.CO2, Random.Range(0.1f, 0.4f), Random.Range(0.6f, 0.95f), co2_count);

        O2Count += o2_count;
        CO2Count += co2_count;
    }

    private Vector2 verifyCursor;

    public override bool Verify() {
        return Verify(verifyCursor);
    }

    public override float GetDangerFactor() {
        return lr.Enabled
            ? Mathf.Max(O2Count / (float) GameMaster.MAX_O2_COUNT, CO2Count / (float) GameMaster.MAX_CO2_COUNT)
            : 0f;
    }

    public override string GetCurrentReadString() {
        return
            $"O<sub>2</sub> Shortage:\n{O2Count:00} / {GameMaster.MAX_O2_COUNT:00}\nCO<sub>2</sub> Pressure:\n{CO2Count:00} / {GameMaster.MAX_CO2_COUNT:00}";
    }

    public override void Reset() {
        O2Count = 0;
        CO2Count = 0;
        O2Container.ResetBreath();
    }

    public bool Verify(Vector2 pos) {
        if (pos.y >= 0.5f) {
            return O2Container.Hit(pos, 0.06f);
        } else {
            return CO2Container.Hit(pos, 0.06f);
        }
    }

    private float screenHeight;

    private void Start() {
        Type = ChallengeType.RR;
        
        screenHeight = GameMaster.Instance.ScreenPosMax.y - GameMaster.Instance.ScreenPosMin.y;
        lr.OnProgressReset += () => { };
        lr.OnCursor += (cursor) => { verifyCursor = cursor; };
        O2Container.OnHit += (index, type) => {
            if (type == BreathType.O2) {
                O2Count--;
            }
        };
        CO2Container.OnHit += (index, type) => {
            if (type == BreathType.CO2) {
                CO2Count--;
            }
        };
    }

    protected override void Update() {
        base.Update();

        UpdateBreathContainerPos();
    }

    public float CurrentLineCenter = -1f;
    private float offset = 0.45f;

    private void UpdateBreathContainerPos() {
        if (Mathf.Abs(CurrentLineCenter - lr.TargetLineCenter) > 0.001f) {
            CurrentLineCenter = Mathf.Lerp(CurrentLineCenter, lr.CurrentLineCenter,
                lr.LineShiftSmoothingFactor * Time.deltaTime);
            var new_pos_o2 = new Vector3(0f,
                (CurrentLineCenter + offset * lr.CurrentLineHeight * lr.LocalHeight) * screenHeight +
                GameMaster.Instance.ScreenPosMin.y, 0);
            var new_pos_co2 = new Vector3(0f,
                (CurrentLineCenter - offset * lr.CurrentLineHeight * lr.LocalHeight) * screenHeight +
                GameMaster.Instance.ScreenPosMin.y, 0);
            O2Container.transform.position = new_pos_o2;
            CO2Container.transform.position = new_pos_co2;
        }
    }

    public void GenerateBreath(BreathType type, float posStart, float posEnd, int count) {
        var arr = GeneratePos(count);
        switch (type) {
            case BreathType.O2:
                for (var i = 0; i < count; i++) {
                    var pos = Mathf.Lerp(posStart, posEnd, arr[i]);
                    O2Container.AddBreathAt(pos);
                }

                break;
            case BreathType.CO2:
                for (var i = 0; i < count; i++) {
                    var pos = Mathf.Lerp(posStart, posEnd, arr[i]);
                    CO2Container.AddBreathAt(pos);
                }

                break;
        }
    }
}