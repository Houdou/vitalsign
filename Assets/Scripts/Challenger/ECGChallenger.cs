using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ECGChallenger : Challenger {
    public HeartContainer HeartContainer;
    public ECGLineRunner lr;
    public int HeartChallengeMax = 4;
    public int HeartChallengeMin = 1;

    public int BrokenHeartCount { get; private set; }

    private List<float> GeneratePos(int count) {
        if (count <= 1) {
            return new List<float> {0.0f};
        }

        var cur = 0f;
        var list = new List<float>();
        for (var i = 0; i < count; i++) {
            list.Add(cur);
            cur += 1f / (count - 1f);
        }

        return list;
    }

    public override void Challenge() {
        if (!lr.Enabled) {
            return;
        }
        
        base.Challenge();
        var heart_count = Random.Range(HeartChallengeMin, HeartChallengeMax);
        GenerateHeart(Random.Range(0.1f, 0.3f), Random.Range(0.7f, 0.9f), heart_count);
        BrokenHeartCount += heart_count;
    }

    public override float GetDangerFactor() {
        return lr.Enabled ? BrokenHeartCount / (float) GameMaster.MAX_BROKEN_HEART_COUNT : 0f;
    }

    public override string GetCurrentReadString() {
        return $"Irregular beats: {BrokenHeartCount:00} / {GameMaster.MAX_BROKEN_HEART_COUNT:00}";
    }

    public override void Reset() {
        HeartContainer.ResetItems();
        BrokenHeartCount = 0;
    }

    public bool Verify(float pos) {
        return HeartContainer.Hit(pos, 0.02f);
    }

    IEnumerator DelayedVerify(float heartPos) {
        var delay = 1f / GameMaster.Instance.RefreshRate * lr.BeatWidth / 2;
        yield return new WaitForSeconds(delay);
        Verify(heartPos);
        currentDelayedVerify = null;
    }

    private float screenHeight;

    private Coroutine currentDelayedVerify = null;

    private void Start() {
        Type = ChallengeType.HR;
        
        screenHeight = GameMaster.Instance.ScreenPosMax.y - GameMaster.Instance.ScreenPosMin.y;
        lr.OnProgressReset += () => { };
        lr.OnBeat += (progress) => {
            var heart_pos = progress + lr.BeatWidth / 2f;

            if (currentDelayedVerify != null) {
                StopCoroutine(currentDelayedVerify);
            }

            currentDelayedVerify = StartCoroutine(DelayedVerify(heart_pos));
        };
        HeartContainer.OnHit += (index) => { BrokenHeartCount--; };
    }

    protected override void Update() {
        base.Update();

        UpdateHeartContainerPos();
    }

    public float CurrentLineCenter = -1f;

    private void UpdateHeartContainerPos() {
        if (Mathf.Abs(CurrentLineCenter - lr.TargetLineCenter) > 0.001f) {
            CurrentLineCenter = Mathf.Lerp(CurrentLineCenter, lr.CurrentLineCenter,
                lr.LineShiftSmoothingFactor * Time.deltaTime);
            var new_pos = new Vector3(0f,
                (CurrentLineCenter + lr.BeatHeight * lr.CurrentLineHeight * lr.LocalHeight * 0.7f) * screenHeight +
                GameMaster.Instance.ScreenPosMin.y, 0);
            HeartContainer.transform.position = new_pos;
        }
    }

    public void GenerateHeart(float posStart, float posEnd, int count) {
        var arr = GeneratePos(count);
        for (var i = 0; i < count; i++) {
            var pos = Mathf.Lerp(posStart, posEnd, arr[i]);

            HeartContainer.AddHeartAt(pos);
            // Register challenge
        }
    }
}