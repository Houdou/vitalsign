using System.ComponentModel;

public enum ChallengeType {
    [Description("Body Temperature")] BT,
    [Description("Heart Rate")] HR,
    [Description("Respiratory rate[")] RR,
    [Description("NIBP")] NIBP
}