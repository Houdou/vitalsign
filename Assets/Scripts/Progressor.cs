﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChallengerSetup {
    public Vector2 BTInterval;
    public Vector2 BTRange;
    public Vector2 HRInterval;
    public Vector2 HRRange;
    public Vector2 RRInterval;
    public Vector2 RRRange;
    public float RRDeviationForce;
    public bool BTEnalbed;
    public bool HREnalbed;
    public bool RREnalbed;
    public int Arrangement;
}

public class ProgressorConfig {
    public static List<ChallengerSetup> Setup = new List<ChallengerSetup> {
        new ChallengerSetup {
            BTEnalbed = true,
            HREnalbed = false,
            RREnalbed = false,
            BTInterval = new Vector2(4f, 8f),
            HRInterval = new Vector2(999f, 999f),
            RRInterval = new Vector2(999f, 999f),
            RRDeviationForce = 15f,
            BTRange = new Vector2(-1, 1),
            HRRange = new Vector2(1, 2),
            RRRange = new Vector2(1, 2),
            Arrangement = 1,
        },
        new ChallengerSetup {
            BTEnalbed = true,
            HREnalbed = true,
            RREnalbed = false,
            BTInterval = new Vector2(4f, 6f),
            HRInterval = new Vector2(6f, 12f),
            RRInterval = new Vector2(999f, 999f),
            RRDeviationForce = 15f,
            BTRange = new Vector2(-2, 2),
            HRRange = new Vector2(1, 3),
            RRRange = new Vector2(1, 2),
            Arrangement = 2,
        },
        new ChallengerSetup {
            BTEnalbed = true,
            HREnalbed = true,
            RREnalbed = true,
            BTInterval = new Vector2(3f, 5f),
            HRInterval = new Vector2(6f, 10f),
            RRInterval = new Vector2(5f, 8f),
            RRDeviationForce = 15f,
            BTRange = new Vector2(-2, 4),
            HRRange = new Vector2(2, 4),
            RRRange = new Vector2(2, 2),
            Arrangement = 3,
        },
        new ChallengerSetup {
            BTEnalbed = true,
            HREnalbed = true,
            RREnalbed = true,
            BTInterval = new Vector2(3f, 5f),
            HRInterval = new Vector2(5f, 8f),
            RRInterval = new Vector2(4f, 6f),
            RRDeviationForce = -5f,
            BTRange = new Vector2(-3, 5),
            HRRange = new Vector2(2, 4),
            RRRange = new Vector2(2, 3),
            Arrangement = 3,
        },
        new ChallengerSetup {
            BTEnalbed = true,
            HREnalbed = true,
            RREnalbed = true,
            BTInterval = new Vector2(3f, 8f),
            HRInterval = new Vector2(5f, 7f),
            RRInterval = new Vector2(4f, 5f),
            RRDeviationForce = -8f,
            BTRange = new Vector2(-4, 6),
            HRRange = new Vector2(3, 4),
            RRRange = new Vector2(2, 3),
            Arrangement = 3,
        },
    };
}

public class Progressor : MonoBehaviour {
    private LevelManager lm;

    public TempChallenger TempChallenger;
    public ECGChallenger ECGChallenger;
    public RRChallenger RRChallenger;

    private void Awake() {
        lm = GetComponent<LevelManager>();
        lm.OnStart += () => {
            UpdateChallenger(ProgressorConfig.Setup[lm.Level - 1]);
            GameTimer = 0f;
            StageTimer = 0f;
        };
    }

    public float StageTimer { get; private set; }
    public float GameTimer { get; private set; }

    private void Update() {
        if (!GameMaster.Instance.Paused && lm.Started) {
            GameTimer += Time.deltaTime;
            StageTimer += Time.deltaTime;

            // Stage up; 
            if (StageTimer >= 30f) {
                StageTimer -= 30f;
                if (lm.LevelUp()) {
                    UpdateChallenger(ProgressorConfig.Setup[lm.Level - 1]);
                }
                
            }
        }
    }

    private void UpdateChallenger(ChallengerSetup setup) {
        lm.UpdateArrangement(setup.Arrangement);

        TempChallenger.TempChallengeMax = (int) setup.BTRange.y;
        TempChallenger.TempChallengeMin = (int) setup.BTRange.x;
        TempChallenger.ChallengeIntervalMax = setup.BTInterval.y;
        TempChallenger.ChallengeIntervalMin = setup.BTInterval.x;
        TempChallenger.AutoChallengeEnabled = setup.BTEnalbed;
        
        ECGChallenger.HeartChallengeMax = (int) setup.HRRange.y;
        ECGChallenger.HeartChallengeMin = (int) setup.HRRange.x;
        ECGChallenger.ChallengeIntervalMax = setup.HRInterval.y;
        ECGChallenger.ChallengeIntervalMin = setup.HRInterval.x;
        ECGChallenger.AutoChallengeEnabled = setup.HREnalbed;
        
        RRChallenger.BreathChallengeMax = (int) setup.RRRange.y;
        RRChallenger.BreathChallengeMin = (int) setup.RRRange.x;
        RRChallenger.ChallengeIntervalMax = setup.RRInterval.y;
        RRChallenger.ChallengeIntervalMin = setup.RRInterval.x;
        RRChallenger.AutoChallengeEnabled = setup.RREnalbed;
        RRChallenger.lr.RRDeviationForce = setup.RRDeviationForce;
    }
}