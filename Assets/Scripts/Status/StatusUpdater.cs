﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class StatusUpdater : MonoBehaviour {
    public Challenger ch;
    public TextMeshProUGUI text;

    public float RefreshRate = 5f;
    private float timer = 0f;

    public void Update() {
        timer -= Time.deltaTime;
        if (timer < 0f) {
            text.text = ch.GetCurrentReadString();
            
            timer += 1f / RefreshRate;
        }
    }
}