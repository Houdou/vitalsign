﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusImage : MonoBehaviour {
    public Image img;
    public Gradient grad;
    public Challenger ch;
    void Update() {
        if (ch.enabled) {
            img.color = grad.Evaluate(ch.GetDangerFactor());
        }
    }
}