﻿using System;
using UnityEngine;
using Utils;

public class GameMaster : MonoBehaviour {
    public static readonly int RESOLUTION = 512;

    public static readonly int MAX_BROKEN_HEART_COUNT = 15;
    public static readonly int MAX_O2_COUNT = 12;
    public static readonly int MAX_CO2_COUNT = 12;

    #region Singleton

    private static GameMaster _instance;
    private static object _lock = new object();

    public static GameMaster Instance {
        get {
            if (applicationIsQuitting) {
                Debug.LogWarning("[Singleton] Instance '" + typeof(GameMaster) +
                                 "' already destroyed on application quit. Returning null.");
                return null;
            }

            lock(_lock) {
                if (_instance != null) return _instance;

                _instance = (GameMaster) FindObjectOfType(typeof(GameMaster));

                if (_instance != null) return _instance;

                var singleton = new GameObject("GameMaster");
                _instance = singleton.AddComponent<GameMaster>();

                DontDestroyOnLoad(singleton);

                Debug.Log("[Singleton] Instance '" + typeof(GameMaster) +
                          "' is created.");

                return _instance;
            }
        }
    }

    private static bool applicationIsQuitting;

    public void OnDestroy() {
        applicationIsQuitting = true;
    }

    #endregion

    public bool Paused { get; private set; } = false;
    [Range(0.01f, 2f)] public float RefreshRate = 0.5f;

    public Vector2 ScreenPosMin { get; private set; }
    public Vector2 ScreenPosMax { get; private set; }

    private void Awake() {
        DetermineScreenPos();
    }

    private void Update() {
        if (Input.GetButtonDown("Cancel")) {
            Application.Quit();
        }
    }

    private void DetermineScreenPos() {
        var min = Camera.main.ScreenToWorldPoint(new Vector3(0.19f * Screen.width, 80, 10f));
        var max = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.81f, Screen.height - 80, 10f));
        ScreenPosMin = min.ToBasePlane();
        ScreenPosMax = max.ToBasePlane();
    }
}